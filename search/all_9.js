var searchData=
[
  ['infostream',['infoStream',['../struct_stream_audio.html#af20f6b1be96849fa7cff72207a14fcf9',1,'StreamAudio']]],
  ['init',['init',['../struct_run_stream.html#afd461a800544d52e7c3899da989ffe44',1,'RunStream']]],
  ['initchangestream',['initChangeStream',['../group___audio_stream__conf.html#ga6cfa1972800cf41779996bc8c0091082',1,'AudioStream_conf.c']]],
  ['initsonic',['initSonic',['../group___audio_sonic.html#ga78fa30e712067e7a1d40c51c73ce54d5',1,'initSonic():&#160;AudioSonic.c'],['../group___audio_sonic.html#ga78fa30e712067e7a1d40c51c73ce54d5',1,'initSonic():&#160;AudioSonic.c']]],
  ['initstreamaudio',['initStreamAudio',['../group___audio_stream.html#gadf200a40a9eb45cfa8cde471f9641041',1,'initStreamAudio():&#160;AudioStream.c'],['../group___audio_stream.html#gadf200a40a9eb45cfa8cde471f9641041',1,'initStreamAudio():&#160;AudioStream.c']]],
  ['irqcallbackaudio',['irqCallbackAudio',['../group__audio__port.html#ga1c68ce300d7bb7ef1630257b815b117f',1,'irqCallbackAudio():&#160;audio_port.c'],['../group__audio__port.html#ga1c68ce300d7bb7ef1630257b815b117f',1,'irqCallbackAudio():&#160;audio_port.c']]],
  ['irqcallbackstream',['irqCallbackStream',['../group__audio__port.html#ga79099158db401dce4b34c1a209bbf78a',1,'irqCallbackStream(AUDIO_BUFF_DMA playstatus):&#160;audio_port.c'],['../group___audio_stream.html#gab51bd711a8266ec5ec8cca0215e3dbd6',1,'irqCallbackStream(AUDIO_BUFF_DMA playstatus):&#160;AudioStream.c']]],
  ['irqeventaudiostream',['irqEventAudioStream',['../group___audio_stream.html#gaa8bbaf520ea25b0aac187d761ece1de6',1,'AudioStream.c']]],
  ['isconverttomono',['isConvertToMono',['../struct_conf_stream_audio.html#ad8da99e1f7d2ddd7c2768e98945a5d79',1,'ConfStreamAudio']]],
  ['isdownsample',['isDownSample',['../struct_conf_stream_audio.html#a280f00210c3784a8bccaf5d9c8b2eb21',1,'ConfStreamAudio']]],
  ['isemptybuffsonic',['isEmptyBuffSonic',['../group___audio_sonic.html#ga690a373bc99fd506ac76a85031ff50b9',1,'isEmptyBuffSonic():&#160;AudioSonic.c'],['../group___audio_sonic.html#ga690a373bc99fd506ac76a85031ff50b9',1,'isEmptyBuffSonic():&#160;AudioSonic.c']]]
];
