var indexSectionsWithContent =
{
  0: "_abcdefghimnprsuvx",
  1: "acersu",
  2: "ar",
  3: "acfgiprsv",
  4: "abcdefghimnprsvx",
  5: "ac",
  6: "ab",
  7: "_",
  8: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Структуры данных",
  2: "Файлы",
  3: "Функции",
  4: "Переменные",
  5: "Перечисления",
  6: "Элементы перечислений",
  7: "Макросы",
  8: "Группы"
};

