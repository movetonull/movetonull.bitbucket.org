var searchData=
[
  ['initchangestream',['initChangeStream',['../group___audio_stream__conf.html#ga6cfa1972800cf41779996bc8c0091082',1,'AudioStream_conf.c']]],
  ['initsonic',['initSonic',['../group___audio_sonic.html#ga78fa30e712067e7a1d40c51c73ce54d5',1,'initSonic():&#160;AudioSonic.c'],['../group___audio_sonic.html#ga78fa30e712067e7a1d40c51c73ce54d5',1,'initSonic():&#160;AudioSonic.c']]],
  ['initstreamaudio',['initStreamAudio',['../group___audio_stream.html#gadf200a40a9eb45cfa8cde471f9641041',1,'initStreamAudio():&#160;AudioStream.c'],['../group___audio_stream.html#gadf200a40a9eb45cfa8cde471f9641041',1,'initStreamAudio():&#160;AudioStream.c']]],
  ['irqcallbackaudio',['irqCallbackAudio',['../group__audio__port.html#ga1c68ce300d7bb7ef1630257b815b117f',1,'irqCallbackAudio():&#160;audio_port.c'],['../group__audio__port.html#ga1c68ce300d7bb7ef1630257b815b117f',1,'irqCallbackAudio():&#160;audio_port.c']]],
  ['irqcallbackstream',['irqCallbackStream',['../group__audio__port.html#ga79099158db401dce4b34c1a209bbf78a',1,'irqCallbackStream(AUDIO_BUFF_DMA playstatus):&#160;audio_port.c'],['../group___audio_stream.html#gab51bd711a8266ec5ec8cca0215e3dbd6',1,'irqCallbackStream(AUDIO_BUFF_DMA playstatus):&#160;AudioStream.c']]],
  ['isemptybuffsonic',['isEmptyBuffSonic',['../group___audio_sonic.html#ga690a373bc99fd506ac76a85031ff50b9',1,'isEmptyBuffSonic():&#160;AudioSonic.c'],['../group___audio_sonic.html#ga690a373bc99fd506ac76a85031ff50b9',1,'isEmptyBuffSonic():&#160;AudioSonic.c']]]
];
