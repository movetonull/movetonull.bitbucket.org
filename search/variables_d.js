var searchData=
[
  ['samplerate',['sampleRate',['../struct_audio_inf.html#a6fa8a5149596e320cf6918c74624e4b6',1,'AudioInf']]],
  ['shift_5ffor_5fdac',['SHIFT_FOR_DAC',['../group___audio_stream.html#gae8f563b5babeca780503033bb4b85a44',1,'AudioStream.c']]],
  ['speed',['speed',['../struct_user_conf_audio.html#a7f7e4724cf57d59513b39c5ecc81adc8',1,'UserConfAudio']]],
  ['storesamplesonic',['storeSampleSonic',['../struct_stream_sonic.html#aee5efbd6a9fec56e553cfd5bb1e63770',1,'StreamSonic']]],
  ['stream',['stream',['../group___audio_sonic.html#ga83fcec6d93f3554e5809bf804bd50a54',1,'AudioSonic.c']]],
  ['streamaudio',['streamAudio',['../group___audio_stream.html#gaf107df4ecb7263ca4cb760835c4c80c5',1,'AudioStream.c']]],
  ['streamsonic',['streamSonic',['../group___audio_sonic.html#gaf71ab95e6148023b5841b505a82e4917',1,'AudioSonic.c']]]
];
