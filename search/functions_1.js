var searchData=
[
  ['changebuff',['changeBuff',['../group___audio_stream__conf.html#gac0eb159c502ebaafc1a72a97b7835db7',1,'AudioStream_conf.c']]],
  ['confaudio',['confAudio',['../group__audio__port.html#ga2382ce459f5ec4f7d9c5775b27c68280',1,'confAudio(uint32_t *buffDMA, uint32_t sizeBuff, uint32_t sampleRate):&#160;audio_port.c'],['../group__audio__port.html#ga2382ce459f5ec4f7d9c5775b27c68280',1,'confAudio(uint32_t *buffDMA, uint32_t sizeBuff, uint32_t sampleRate):&#160;audio_port.c']]],
  ['confchangestream',['confChangeStream',['../group___audio_stream__conf.html#gae2941bbccc87ee0766faa87250b69d43',1,'AudioStream_conf.c']]],
  ['configstreamaudio',['configStreamAudio',['../group___audio_stream.html#gab3800d818c63fc92d12ebc431d3970bf',1,'configStreamAudio(AudioInf *audioInf):&#160;AudioStream.c'],['../group___audio_stream.html#gab3800d818c63fc92d12ebc431d3970bf',1,'configStreamAudio(AudioInf *_audioInf):&#160;AudioStream.c']]],
  ['confsonic',['confSonic',['../group___audio_sonic.html#ga1436a8c522c9d8a281196d850629b628',1,'confSonic(AudioInf *_audioInf):&#160;AudioSonic.c'],['../group___audio_sonic.html#ga1436a8c522c9d8a281196d850629b628',1,'confSonic(AudioInf *_audioInf):&#160;AudioSonic.c']]],
  ['convertbuff',['convertBuff',['../group___audio_convert.html#gab2f3276ea1082763f53019de00a79cba',1,'convertBuff(int16_t *buff):&#160;AudioConvert.c'],['../group___audio_convert.html#gab2f3276ea1082763f53019de00a79cba',1,'convertBuff(int16_t *buff):&#160;AudioConvert.c']]]
];
