var searchData=
[
  ['samplerate',['sampleRate',['../struct_audio_inf.html#a6fa8a5149596e320cf6918c74624e4b6',1,'AudioInf']]],
  ['shift_5ffor_5fdac',['SHIFT_FOR_DAC',['../group___audio_stream.html#gae8f563b5babeca780503033bb4b85a44',1,'AudioStream.c']]],
  ['shiftbuffsonic',['shiftBuffSonic',['../group___audio_sonic.html#gadbaf1dac764df711850137758e507007',1,'shiftBuffSonic():&#160;AudioSonic.c'],['../group___audio_sonic.html#gadbaf1dac764df711850137758e507007',1,'shiftBuffSonic():&#160;AudioSonic.c']]],
  ['size_5fbuff_5fsonic',['SIZE_BUFF_SONIC',['../group___audio_sonic.html#ga649ec41f1442d49d0265aee33abc0f05',1,'AudioSonic.c']]],
  ['speed',['speed',['../struct_user_conf_audio.html#a7f7e4724cf57d59513b39c5ecc81adc8',1,'UserConfAudio']]],
  ['startaudio',['startAudio',['../group__audio__port.html#gaafd6cebd77ca840cfa41b34e3b043b2d',1,'startAudio():&#160;audio_port.c'],['../group__audio__port.html#gaafd6cebd77ca840cfa41b34e3b043b2d',1,'startAudio():&#160;audio_port.c'],['../group___audio_stream.html#gabe8acabf2811d8faa765cafb9becb570',1,'startAudio():&#160;AudioStream.c']]],
  ['stopaudio',['stopAudio',['../group__audio__port.html#ga7002820c519a681f100f398a50e54ba9',1,'stopAudio():&#160;audio_port.c'],['../group__audio__port.html#ga7002820c519a681f100f398a50e54ba9',1,'stopAudio():&#160;audio_port.c'],['../group___audio_stream.html#ga1d4ab230f4e9e654ed2d82a503ca3d22',1,'stopAudio():&#160;AudioStream.c']]],
  ['storesamplesonic',['storeSampleSonic',['../struct_stream_sonic.html#aee5efbd6a9fec56e553cfd5bb1e63770',1,'StreamSonic']]],
  ['stream',['stream',['../group___audio_sonic.html#ga83fcec6d93f3554e5809bf804bd50a54',1,'AudioSonic.c']]],
  ['streamaudio',['StreamAudio',['../struct_stream_audio.html',1,'StreamAudio'],['../group___audio_stream.html#gaf107df4ecb7263ca4cb760835c4c80c5',1,'streamAudio():&#160;AudioStream.c']]],
  ['streamsonic',['StreamSonic',['../struct_stream_sonic.html',1,'StreamSonic'],['../group___audio_sonic.html#gaf71ab95e6148023b5841b505a82e4917',1,'streamSonic():&#160;AudioSonic.c']]]
];
