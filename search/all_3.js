var searchData=
[
  ['changebuff',['changeBuff',['../struct_run_stream.html#aa5b46e0bd178afeea0597acbcd55882b',1,'RunStream::changeBuff()'],['../group___audio_stream__conf.html#gac0eb159c502ebaafc1a72a97b7835db7',1,'changeBuff():&#160;AudioStream_conf.c']]],
  ['comm',['comm',['../struct_event_stream.html#a83ffa6b5fd574527f73788e11e2410c7',1,'EventStream']]],
  ['command_5faudio_5fstream',['COMMAND_AUDIO_STREAM',['../_audio_stream_8h.html#a67e72bd68fdc1e8f2b891668c9951e99',1,'AudioStream.h']]],
  ['conf',['conf',['../struct_run_stream.html#a6c7e90e5415c48c17a6b273fdaccb068',1,'RunStream']]],
  ['confaudio',['confAudio',['../struct_event_stream.html#a77f54f4f4efba0bc10a45161d20de426',1,'EventStream::confAudio()'],['../group__audio__port.html#ga2382ce459f5ec4f7d9c5775b27c68280',1,'confAudio(uint32_t *buffDMA, uint32_t sizeBuff, uint32_t sampleRate):&#160;audio_port.c'],['../group__audio__port.html#ga2382ce459f5ec4f7d9c5775b27c68280',1,'confAudio(uint32_t *buffDMA, uint32_t sizeBuff, uint32_t sampleRate):&#160;audio_port.c']]],
  ['confchangestream',['confChangeStream',['../group___audio_stream__conf.html#gae2941bbccc87ee0766faa87250b69d43',1,'AudioStream_conf.c']]],
  ['configstreamaudio',['configStreamAudio',['../group___audio_stream.html#gab3800d818c63fc92d12ebc431d3970bf',1,'configStreamAudio(AudioInf *audioInf):&#160;AudioStream.c'],['../group___audio_stream.html#gab3800d818c63fc92d12ebc431d3970bf',1,'configStreamAudio(AudioInf *_audioInf):&#160;AudioStream.c']]],
  ['confsonic',['confSonic',['../group___audio_sonic.html#ga1436a8c522c9d8a281196d850629b628',1,'confSonic(AudioInf *_audioInf):&#160;AudioSonic.c'],['../group___audio_sonic.html#ga1436a8c522c9d8a281196d850629b628',1,'confSonic(AudioInf *_audioInf):&#160;AudioSonic.c']]],
  ['confstream',['confStream',['../group___audio_convert.html#ga036871e3cc9327e243f72653bf518a50',1,'confStream():&#160;AudioConvert.c'],['../group___audio_convert.html#ga036871e3cc9327e243f72653bf518a50',1,'confStream():&#160;AudioConvert.c']]],
  ['confstreamaudio',['ConfStreamAudio',['../struct_conf_stream_audio.html',1,'']]],
  ['convertbuff',['convertBuff',['../group___audio_convert.html#gab2f3276ea1082763f53019de00a79cba',1,'convertBuff(int16_t *buff):&#160;AudioConvert.c'],['../group___audio_convert.html#gab2f3276ea1082763f53019de00a79cba',1,'convertBuff(int16_t *buff):&#160;AudioConvert.c']]]
];
